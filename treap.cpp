#include <algorithm>
#include <iostream>
#include <sstream>
#include <ctime>
#include <string>
#include <vector>
#include <random>

using std::cin;
using std::cout;
using std::istringstream;
using std::pair;
using std::string;
using std::vector;

template <typename T>
class Treap {
  struct Node {
    Node *left, *right;
    T data;
    int size;
    int prior;
    Node() = default;
    Node(const T &data);
  };
  Node *root;
  Node *merge(Node *left, Node *right);
  pair<Node *, Node *> split(Node *node, size_t key);
  void update(Node *node);
  size_t getSize(Node *node);
  static std::mt19937 generator;  
 public:
  void InsertAt(int position, const T &value);
  void DeleteAt(int position);
  void DeleteAtRange(int leftPosition, int rightPosition);
  T GetAt(int position);
  Treap();
};

int main() {
  int queryCount;
  cin >> queryCount;
  Treap<string> tree;
  for (int query = 0; query < queryCount; query++) {
    string s;
    getline(cin, s);
    istringstream iss(s);
    if (s[0] == '+') {
      int position;
      string value;
      iss >> position >> value;
      tree.InsertAt(position, value);
    }
    if (s[0] == '-') {
      int pos1, pos2;
      iss >> pos1;
      if (iss >> pos2)
        tree.DeleteAtRange(pos1, pos2);  // Поддержка одного и двух запросов
      else
        tree.DeleteAt(pos1);
    }
    if (s[0] == '?') {
      int pos;
      iss >> pos;
      cout << tree.GetAt(pos) << std::endl;
    }
  }
}

template <typename T>
std::mt19937 Treap<T>::generator = std::mt19937(time(0));

template <typename T>
Treap<T>::Treap() : root(nullptr) {
}

template <typename T>
Treap<T>::Node::Node(const T &data)
    : data(data), left(nullptr), right(nullptr), size(1), prior(generator()) {}

template <typename T>
pair<typename Treap<T>::Node *, typename Treap<T>::Node *> Treap<T>::split(
    typename Treap<T>::Node *node, size_t key) {
    if (node == nullptr)
        return {nullptr, nullptr};
    if (getSize(node->left) >= key) {
        auto splited = split(node->left, key);
        node->left = splited.second;
        update(node);
        return {splited.first, node};
    } else {
        auto splited = split(node->right, key - getSize(node->left) - 1);
        node->right = splited.first;
        update(node);
        return {node, splited.second};
    }
}

template <typename T>
typename Treap<T>::Node *Treap<T>::merge(typename Treap<T>::Node *left, typename Treap<T>::Node *right) {
    if (left == nullptr)
        return right;
    if (right == nullptr)
        return left;
    if (left->prior > right->prior) {
        left->right = merge(left->right, right);
        update(left);
        return left;
    } else {
        right->left = merge(left, right->left);
        update(right);
        return right;
    }
}

template <typename T>
void Treap<T>::update(typename Treap<T>::Node *node) {
    if (node != nullptr)
        node->size = getSize(node->left) + getSize(node->right) + 1;
}

template <typename T>
size_t Treap<T>::getSize(typename Treap<T>::Node *node) {
    if (node != nullptr)
        return node->size;
    else
        return 0;
}

template <typename T>
void Treap<T>::InsertAt(int position, const T &data) {
    Node *node = new Node(data);
    auto splited = split(root, position);
    root = merge(merge(splited.first, node), splited.second);
}

template <typename T>
void Treap<T>::DeleteAt(int position) {
    auto splited = split(root, position);
    auto secondSplited = split(splited.second, 1);
    root = merge(splited.first, secondSplited.second);
}

template <typename T>
void Treap<T>::DeleteAtRange(int leftPosition, int rightPosition) {
    auto splited = split(root, leftPosition);
    auto secondSplited = split(splited.second, rightPosition - leftPosition + 1);
    root = merge(splited.first, secondSplited.second);
}

template<typename T>
T Treap<T>::GetAt(int position) {
    auto splited = split(root, position);
    auto secondSplited = split(splited.second, 1);
    T ans = secondSplited.first->data;
    splited.second = merge(secondSplited.first, secondSplited.second);
    root = merge(splited.first, splited.second);
    return ans;
}
